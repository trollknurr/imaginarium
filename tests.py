#!/usr/bin/python
# -*- coding: utf-8 -*-
import math
import cmath

from helpers import Time


__author__ = 'trollknurr'

import json
import os
import numpy as np
import unittest
from locator import Locator
from receivers.radio import Receiver
import matplotlib.pyplot as plt


class LocatorTests(unittest.TestCase):
    def setUp(self):
        self.test_config = {
            'X_START': 10,
            'Z_START': 10,
            'X_SIZE': 2,
            'Z_SIZE': 2,
            'SIMPLE_STEP': 0.001,
            'LAMBDA': 0.03,
            'ROTATION_SPEED': 360,
            'DNA_GAIN': 10,
            'DNA_TYPE': 0,
        }

    def test_dna_angle(self):
        CURRENT_DIR = os.path.abspath(os.path.dirname(__file__))
        locator = Locator(self.test_config)
        angles = open(os.path.join(CURRENT_DIR, 'test_assets/dna_angles.dat'), 'r')
        for x_angle in locator.x_angles:
            print(x_angle)
            self.assertAlmostEqual(x_angle, float(angles.readline()), 3)

    def test_dna_values(self):
        CURRENT_DIR = os.path.abspath(os.path.dirname(__file__))
        locator = Locator(self.test_config)
        values = open(os.path.join(CURRENT_DIR, 'test_assets/dna_values.dat'), 'r')
        for x_value in locator.x_values:
            print(x_value)
            self.assertAlmostEqual(x_value, float(values.readline()), 3)

class RadioTest(unittest.TestCase):
    CURRENT_DIR = os.path.abspath(os.path.dirname(__file__))

    def setUp(self):
        config = json.load(open(os.path.join(self.CURRENT_DIR, 'test_assets', 'config.json'), 'r'))
        self.radio = Receiver(config)
        self.radio.blank()

    def _module(self, z):
        return math.sqrt(pow(z.real, 2) + pow(z.imag, 2))

    def test_heaviside(self):
        test_arr = np.random.randint(5, 15, 20)
        out_arr = np.zeros_like(test_arr)
        shift = 10
        for index, el in enumerate(test_arr):
            out_arr[index] = el*(self.radio._heaviside(index) - self.radio._heaviside(index - shift))
        self.assertEqual(out_arr[11], 0)

    def test_emitted_signal_model(self):
        out = []
        for t in range(self.radio.receive_array_len):
            out.append(self.radio._harm_signal_function(t * self.radio.sampling_time_delta))
        self.assertGreater(self._module(out[0]), 0)
        pulse_end_index = int(self.radio.pulse_duration / self.radio.sampling_time_delta)
        self.assertEqual(out[pulse_end_index + 1], 0)

    def test_received_signal_model(self):
        out = []
        pulse_end_index = int(self.radio.pulse_duration / self.radio.sampling_time_delta)
        tz = 2 * pulse_end_index * self.radio.sampling_time_delta
        for t in range(self.radio.receive_array_len):
            out.append(self.radio._harm_signal_function(t * self.radio.sampling_time_delta - tz))

        self.assertEqual(out[0], 0)
        self.assertNotEqual(self._module(out[2 * pulse_end_index + 1]), 0)
        self.assertEqual(out[3 * pulse_end_index + 1], 0)

    def test_received_signal_model_with_dopler(self):
        out = []
        pulse_end_index = int(self.radio.pulse_duration / self.radio.sampling_time_delta)
        tz = 2 * pulse_end_index * self.radio.sampling_time_delta
        for t in range(self.radio.receive_array_len):
            out.append(self.radio._harm_signal_function_with_dopler(t * self.radio.sampling_time_delta - tz, 15))

        self.assertEqual(out[0], 0)
        self.assertNotEqual(self._module(out[2 * pulse_end_index + 1]), 0)
        self.assertEqual(out[3 * pulse_end_index + 1], 0)

    def test_received_signal_dopler_work(self):
        r = 5e+2
        prev_value = 0
        values = []
        time = Time(self.radio.pulse_repeat_time, self.radio.sampling_time_delta, 100, 0.0002)
        for k in range(0, 100, 1):
            new_r = r - 30 * k * time.get_world_time_step()
            tz = 2 * new_r / 3.e+8
            tz_index = int(tz / self.radio.sampling_time_delta)
            t = time.get_time(k, tz_index + 2) - (tz + time.get_time(k, 0))
            value = self.radio._harm_signal_function_with_dopler(t, 30)
            values.append(cmath.phase(value))
        plt.plot(values)
        plt.show()
            # self.assertNotEqual(value, prev_value)
            # prev_value = value