#!/usr/bin/python
#  -*- coding: utf-8 -*-

__author__ = 'trollknurr'

import os
import json
import argparse

try:
    import matplotlib.pyplot as plt
    import matplotlib.animation as animation
except ImportError:
    pass

from helpers import *
from targets import TargetWithAproxConstSpeed, TargetWithApproxConstAcceleration
from locator import Locator
from receivers import RadioReceiver, LfmReceiver
from writers import FileWriter, ServerWriter


class WorldController(object):
    """
    Класс, управляющий всем происходяшим.
    Считывает конфиг, создает экземпляры целей, локатор и приемник и писателя в файл
    Метод run запускает моделирование мира
    Принимает опциональный аргумент plot - если установлен matplotlib вместо файла принимаемый сигнал будет динамически
    отображаться на графике
    """

    def __init__(self, file_name=None, configfile='config.json', server_mode=False):
        """
        Инициализация класса
        :param file_name:
        :param configfile:
        """
        config = self.read_config(configfile)
        print('Initializing')
        self.apply_config(config)
        print('Config applied')
        # Инициализируем запись в файл
        if server_mode:
            self.writer = ServerWriter(self.receiver.receive_array_len, self.steps_in_frame)
        else:
            self.writer = FileWriter(self.receiver.receive_array_len, self.steps_in_frame, file_name)


    def apply_config(self, config):
        """
        Настраиваем программу в соответствии с переданныи конфигурационном файлом
        :param config: Словарь с конфигурацией
        """
        #Рассчитываем период дескритизации приемника
        self.sampling_timedelta = 1. / config['FREQUENCY']
        #Тип записываемых данных, доступных для numpy(ex. 'int16')
        self.output_data_type = config["WRITE_MODE"]
        # Инициализируется соответствующий класс приемника
        receiver_class = {0: RadioReceiver,
                          1: LfmReceiver}.get(config["SIGNAL_TYPE"], 0)
        self.receiver = receiver_class(config)
        # Локатор
        self.locator = Locator(config)
        # В кадре один оборот
        self.steps_total = config['LOCATOR_ENCODER_RESOLUTION']
        # Учитываем прореживание
        try:
            # Шаг прореживания
            self.step_with_thinning = 1 * config['THINNING']
            self.steps_in_frame = int(self.steps_total / self.step_with_thinning)
        except KeyError:
            self.angle_delta = (2 * np.pi) / config["LOCATOR_ENCODER_RESOLUTION"]
            # Шаг прореживания
            self.step_with_thinning = int((self.locator.speed * self.receiver.pulse_repeat_time * config[
                "PULSE_ACCUMULATION_RATE"]) / self.angle_delta)
            self.steps_in_frame = int(self.steps_total / self.step_with_thinning)

        self.angle_delta = (2 * np.pi) / self.steps_in_frame
        # Рассчитываем период моделирование мира
        self.world_time_delta = self.angle_delta / self.locator.speed
        # Глобальное время
        self.world_time = Time(self.receiver.pulse_repeat_time, self.receiver.sampling_time_delta, self.steps_in_frame,
                               self.world_time_delta)
        # Грязный хак, но приемнику нужно получать время
        self.receiver.time = self.world_time
        # Создаем массив целей
        self.targets = list()
        # Используем модель в зависимости от параметра
        target_classes = {
            'speed_sigma': TargetWithAproxConstSpeed,
            'acceleration_sigma': TargetWithApproxConstAcceleration
        }
        # Создаем цели
        for target_config in config['TARGETS']:
            target_class = None
            for param, cls in target_classes.iteritems():
                if param in target_config:
                    target_class = cls
                    break
            else:
                # Если конфиг неправильный или старый, работаем с обычной линейной моделью
                target_config['speed_sigma'] = 0
                target_class = TargetWithAproxConstSpeed
            self.targets.append(target_class(world_time_delta=self.world_time_delta, **target_config))


    def run(self):
        """
        Начинаем генерацию и продолжаем до тех пор, пока не прервут
        """
        print("Total steps in frame {0}".format(self.steps_in_frame))
        print("Begin generation")
        while True:
            # Для интерактивности выводим текщий угол и его код
            print("Begin new overview")
            # Пишем заголовок кадра
            self.writer.new_frame()
            for direction_number in xrange(0, self.steps_total, self.step_with_thinning):
                # Всем целям подвинуться!
                map(lambda target: target.update(), self.targets)
                # Локатору повернуться!
                self.locator.spin(self.world_time_delta)
                # Применику приготовиться
                self.receiver.blank()
                # Проверяем каждую цель
                for target in self.targets:
                    # Усиление локатора для цели
                    target_loc_gain = self.locator.gain_for_target(target)
                    # Если усиление маленькое или ноль, считаем что цель не видим
                    if target_loc_gain:
                        # Принимаем сигнал от цели
                        self.receiver.receive(direction_number / self.step_with_thinning, target_loc_gain, target,
                                              self.locator.locator_angle)
                # После проверки всех целей, записываем результирующий массив
                self.writer.write(direction_number,  # текущий угол
                                  np.reshape(
                                      np.array((self.receiver.received_data.real, self.receiver.received_data.imag),
                                               dtype=np.dtype(self.output_data_type)).T, -1).tostring())
            print("Overview ended")


    def down(self):
        """
        Остановка генерации и выход из программы
        """
        self.writer.down()  # Закрываем выходной поток


    def read_config(self, config_file_name):
        """
        Чтение конфига, удаление комментариев, преобразование json в понятный питону dict
        """
        with open(os.path.join(os.path.abspath(os.path.dirname(__file__)), config_file_name), 'r') as config_file:
            config = json.load(config_file)
        return config


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--file', '-f', type=str, help='Output filename')
    parser.add_argument('--server', '-s', action='store_true', help='Server mode')
    args = parser.parse_args()
    if args.file:
        w = WorldController(file_name=args.file)
        try:
            w.run()
        except KeyboardInterrupt:
            print "Going down"
            w.down()
    elif args.server:
        w = WorldController(server_mode=True)
        try:
            w.run()
        except KeyboardInterrupt:
            print "Going down"
            w.down()
    else:
        print 'Provide file_name'