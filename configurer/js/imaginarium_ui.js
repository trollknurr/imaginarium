$('#saveBtn').click(extractData);

//Menu headers
var headers = $('.nav-tabs').children();

for (var i=0;i<headers.length;i++) {
    $(headers[i]).click(function() {
        for (var i=0;i<headers.length;i++) {
            if (this == headers[i]) {
                $($(headers[i]).data('table-id')).show();
                $(headers[i]).addClass('active')
            } else {
                $($(headers[i]).data('table-id')).hide();
                $(headers[i]).removeClass('active');
            }
        }
    });

}

// Antenna table
$('#DNA_TYPE').prop("selectedIndex", -1);

$('#DNA_TYPE').change(function() {
    if (this.value == 0) {
        $('#simpleDnaInput').show();
        $('#tableXDnaInput').hide();
        $('#tableZDnaInput').hide();
    } else {
        $('#simpleDnaInput').hide();
        $('#tableXDnaInput').show();
        $('#tableZDnaInput').show();
    }
});

$('#moreXDnaInputs').click(function() {
   $('#tableXDnaCloneInputs').clone().appendTo('#xDnaForm').show();
});
$('#moreZDnaInputs').click(function() {
   $('#tableZDnaCloneInputs').clone().appendTo('#zDnaForm').show();
});

//Station table
$('#moreStationAttenuatorInputs').click(function() {
   $('#stationAttenuatorInputs').clone().appendTo('#stationAttenuatorForm').show();
});

//Target table
$('#moreTargetForm').click(function() {
    $('#targetForm').clone().appendTo('#targetsTable').show();
});

//Signal table
$('#SIGNAL_TYPE').prop("selectedIndex", -1);

$('#SIGNAL_TYPE').change(function() {
    if (this.value == 1) {
        $('#lfmSignalForm').show();
        $('#tableSignalForm').hide();
    } else if (this.value == 0) {
        $('#lfmSignalForm').hide();
        $('#tableSignalForm').hide();
    } else if (this.value == 2) {
        $('#lfmSignalForm').hide();
        $('#tableSignalForm').show();
    }
});

$('#moreTableSignalInputs').click(function(){
    $('#tableSignalInput').clone().appendTo('#tableSignalFormTd').show();
});