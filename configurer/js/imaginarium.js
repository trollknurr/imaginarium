// Check for the various File API support.
if (window.File && window.FileReader && window.FileList && window.Blob) {
  // Great success! All the File APIs are supported.
} else {
  alert('The File APIs are not fully supported in this browser.');
}

var keys = ["WRITE_MODE", "STATION_POWER", "LOCATOR_ENCODER_RESOLUTION", "STATION_ATTENUATOR", "FREQUENCY", "FREQUENCY_PREFIX", "TARGETS", "SIGNAL_TYPE", "LFM_SIGNAL_SLOPE", "TABLE_SIGNAL", "PULSE_DURATION", "DNA_TYPE", "SIMPLE_STEP", "DNA_TABLE_AZ", "DNA_TABLE_PL", "X_SIZE", "Y_SIZE", "ROTATION_SPEED", "LAMBDA", "DNA_GAIN", "RECEIVER_NOISE", "RECEIVER_TRACT", "ANTENNA_TRACT"];
var json_data;

function extractData() {
    var data = new Object();
    for (var i in keys) {
        if (keys[i] == 'DNA_TABLE_AZ') {
            if ($('#DNA_TYPE').val() == 1){
                var dna = new Array();
                $('#xDnaForm').children().map(function () {
                    var obj = new Object();
                    obj['angle'] = +$(this).find('input[name="x_angle[]"]').val();
                    obj['value'] = +$(this).find('input[name="x_value[]"]').val();
                    dna.push(obj);
                })
                data['DNA_TABLE_AZ'] = dna
            }
        } else if (keys[i] == 'DNA_TABLE_PL'){
            if ($('#DNA_TYPE').val() == 1){
                var dna = new Array();
                $('#zDnaForm').children().map(function () {
                    var obj = new Object();
                    obj['angle'] = +$(this).find('input[name="z_angle[]"]').val();
                    obj['value'] = +$(this).find('input[name="z_value[]"]').val();
                    dna.push(obj);
                })
                data['DNA_TABLE_PL'] = dna
            }
        } else if(keys[i] == 'STATION_ATTENUATOR') {
            var att = new Array();
            $('#stationAttenuatorForm').children().map(function () {
                var obj = new Object();
                obj['range'] = +$(this).find('[name="range[]"]').val();
                obj['reduce'] = +$(this).find('[name="reduce[]"]').val();
                att.push(obj);
            })
            data['STATION_ATTENUATOR'] = att;
        }
        else if (keys[i] == 'TARGETS') {
            var targets = new Array();
            $('form.targetForm').map(function () {
                var obj = new Object();
                $(this).find('input').map(function() {
                    obj[$(this).attr('name')] = +$(this).val();
                });
                targets.push(obj);
            })
            data['TARGETS'] = targets;

        } else if (keys[i] == 'TABLE_SIGNAL') {
            if ($('#SIGNAL_TYPE').val() == 2) {
                var signal = new Array();
                $('#tableSignalFormTd').children().map(function() {
                    signal.push(+$(this).val());
                })
            }
            data['TABLE_SIGNAL'] = signal;
        } else {
         data[keys[i]] = +$('#' + keys[i]).val();
         if (isNaN(data[keys[i]])) {
             data[keys[i]] = $('#' + keys[i]).val();
         }
        }
    }
    data = JSON.stringify(data);
    console.log(data);
    var newWindow = window.open();
    newWindow.document.write(data);
    newWindow.document.close();
}

function loadData(json_data) {
    for (var key in json_data) {
        if (key == 'DNA_TABLE_AZ') {
            if (json_data['DNA_TYPE'] == 1){
                $('[name="x_value[]"]').val(json_data['DNA_TABLE_AZ'][0]['value']);
                $('[name="x_angle[]"]').val(json_data['DNA_TABLE_AZ'][0]['angle']);
                for (var i = 1; i < json_data[key].length; i++) {
                    var new_input = $('#tableXDnaCloneInputs').clone()
                    new_input.find('[name="x_value[]"]').val(json_data['DNA_TABLE_AZ'][i]['value']);
                    new_input.find('[name="x_angle[]"]').val(json_data['DNA_TABLE_AZ'][i]['angle']);
                    new_input.appendTo('#xDnaForm');
                }
            }
        } else if (key == 'DNA_TABLE_PL'){
            if (json_data['DNA_TYPE'] == 1){
                $('[name="z_value[]"]').val(json_data['DNA_TABLE_PL'][0]['value']);
                $('[name="z_angle[]"]').val(json_data['DNA_TABLE_PL'][0]['angle']);
                for (var i = 1; i < json_data[key].length; i++) {
                    var new_input = $('#tableZDnaCloneInputs').clone()
                    new_input.find('[name="z_value[]"]').val(json_data['DNA_TABLE_PL'][i]['value']);
                    new_input.find('[name="z_angle[]"]').val(json_data['DNA_TABLE_PL'][i]['angle']);
                    new_input.appendTo('#zDnaForm');
                }
            }
        } else if(key == 'STATION_ATTENUATOR') {
            $('#stationAttenuatorInputs').find('[name="range[]"]').val(json_data[key][0]['range']);
            $('#stationAttenuatorInputs').find('[name="reduce[]"]').val(json_data[key][0]['reduce']);
            for (var i = 1; i < json_data[key].length; i++) {
                var new_input = $('#stationAttenuatorInputs').clone();
                new_input.find('[name="range[]"]').val(json_data[key][i]['range']);
                new_input.find('[name="reduce[]"]').val(json_data[key][i]['reduce']);
                new_input.appendTo('#stationAttenuatorForm');
            }
        }
        else if (key == 'TARGETS') {
            if (json_data[key].length > 0) {
                for (var sub_key in json_data[key][0]) {
                    $('.targetForm').find('[name="' + sub_key + '"]').val(json_data[key][0][sub_key]);
                }
            }
            if (json_data[key].length > 1) {
                for (var i = 1; i < json_data[key].length; i++){
                    var new_form = $('#targetForm').clone();
                    for (var sub_key in json_data[key][i]) {
                        new_form.find('[name="' + sub_key + '"]').val(json_data[key][i][sub_key]);
                    }
                    new_form.appendTo('#targetsTable');
                }

            }
        } else if (key == 'TABLE_SIGNAL') {
            if (json_data['SIGNAL_TYPE'] == 2) {
                var input = $('#tableSignalInput');
                input.hide();
                for (var i in json_data['TABLE_SIGNAL']) {
                    var new_input = input.clone();
                    new_input.val(json_data['TABLE_SIGNAL'][i]);
                    new_input.appendTo('#tableSignalFormTd');
                    new_input.show();
                }
            }
        } else {
            $('#' + key).val(json_data[key]);
            $('#' + key).change();
        }
    }

}

$("#fileOpen").click(function(event) {
    event.preventDefault();
    var file = $('#configFileInput')[0].files[0];
    var reader = new FileReader();
    reader.onload = function () {
        json_data = JSON.parse(reader.result);
        loadData(json_data);
    }
    reader.readAsText(file);
});
