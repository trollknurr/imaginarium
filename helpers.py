#!/usr/bin/python
# -*- coding: utf-8 -*-
from numpy import random
import unittest

__author__ = 'trollknurr'

import numpy as np

prefixis = {
    'MHz': 6,
    'Mhz': 6,
    'Ghz': 9,
    'GHz': 9,
    'kHz': 3,
    'khz': 3,
    'Hz': 1,
    'hz': 1
}


class Time(object):
    """
    Класс, предоставляющий отсчеты времени с учетом скорости вращения антенны и частоты АЦП
    """

    def __init__(self, repeat_time, sampling_time, directions_number, direction_time):
        """
        Функция конструктор
        :param repeat_time: период повторения импульсов
        :param sampling_time: период дескритизации АЦП
        :param directions_number: количество различаемых угловых направлений (с учетом прореживания)
        :param direction_time: время стояния в одном угловом направлении
        """

        #service vars
        self.n = int(repeat_time / sampling_time)
        self.k = directions_number

        self.world_time_step = direction_time
        self.sampling_time = sampling_time

    def get_time(self, direction_number, count_number):
        """
        Функция возвращет значение времени по заданным значениям, если таких значений нет - возвращает IndexError
        :param direction_number: номер углового направления
        :param count_number: номер отсчета в массиве реаилизация принятого сигнала
        """
        if self.k <= direction_number or self.n <= count_number:
            raise IndexError
        return direction_number * self.world_time_step + count_number * self.sampling_time

    def get_world_time_step(self):
        """
        Функция возвращает значение периода моделированя мира
        """
        return self.world_time_step


class TimeTest(unittest.TestCase):
    def setUp(self):
        self.time = Time(1e-5, 2.5e-9, 100, 0.01)

    def test_time_world_step(self):
        self.assertEqual(self.time.get_world_time_step(), 0.01)

    def test_time_count_right(self):
        for i in range(10):
            n = random.randint(0, self.time.n)
            k = random.randint(0, 100)
            self.assertEqual(self.time.get_time(k, n), k * 0.01 + n * 2.5e-9)

    def test_time_raises_error(self):
        self.assertRaises(IndexError, self.time.get_time, self.time.n * 2, 10)
        self.assertRaises(IndexError, self.time.get_time, self.time.n - 1, 1000)

    def test_manual_couted_time(self):
        self.assertAlmostEqual(self.time.get_time(10, 2667), 0.1, 2)
