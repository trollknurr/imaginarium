#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'trollknurr'
import random
from .base import BaseTarget


class TargetWithAproxConstSpeed(BaseTarget):
    """
    Модель с примерно постоянной скоростью движения
    :param targetparams: словарь конфигурации
    """

    def __init__(self, **targetparams):
        super(TargetWithAproxConstSpeed, self).__init__(**targetparams)
        self.speed_sigma = targetparams['speed_sigma']
        self.speed_mu = targetparams['mu']

    def update(self):
        """
        Передвигаем цель в соответствии с моделью движения
        """
        random_sample = random.gauss(self.speed_mu, self.speed_sigma)
        for coord, speed in zip(self.cart_coord.keys(), self.speed_for_coord.keys()):
            self.cart_coord[coord] += self.speed_for_coord[speed] * self.time_delta + random_sample * self.time_delta_sq
            self.speed_for_coord[speed] += self.time_delta * random_sample

        self.translate_to_sph()
