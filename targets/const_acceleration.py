#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'trollknurr'

import random
from .base import BaseTarget


class TargetWithApproxConstAcceleration(BaseTarget):
    """
    Модель с примерно постоянным ускорением
    :param targetparams: словарь конфигурации
    """

    def __init__(self, **targetparams):
        super(TargetWithApproxConstAcceleration, self).__init__(**targetparams)
        self.acceleration_sigma = targetparams['acceleration_sigma']
        self.acceleration_mu = targetparams['mu']
        self.acceleration = {
            'ax': targetparams['ax'],
            'ay': targetparams['ay'],
            'az': targetparams['az']
        }

    def update(self):
        """
        Обновляем положение в соответствии с формулой приведенной в документации
        """
        random_sample = random.gauss(self.acceleration_mu, self.acceleration_sigma)
        for coord, speed, acc in zip(self.cart_coord.keys(), self.speed_for_coord.keys(), self.acceleration.keys()):
            self.cart_coord[coord] += self.speed_for_coord[speed] * self.time_delta +\
                                      (self.acceleration[acc] + random_sample) * self.time_delta_sq / 2.
            self.speed_for_coord[speed] += (self.acceleration[acc] + random_sample) * self.time_delta
            self.acceleration[acc] += random_sample

        self.translate_to_sph()