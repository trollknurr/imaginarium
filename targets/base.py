#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'trollknurr'

import math
import numpy as np


class BaseTarget(object):
    """
    Класс представляет собой цель, которая может двигаться. Цель имеет флюктуирующую ЭПР, а так же некое своё
    собственное усиление, задаваемое в зависимости от расстояния
    """

    def __init__(self, **targetparams):
        self.cart_coord = {'x': float(targetparams['x0']),
                           'y': float(targetparams['y0']),
                           'z': float(targetparams['z0'])}

        self.speed_for_coord = {'Vx': targetparams['Vx'],
                                'Vy': targetparams['Vy'],
                                'Vz': targetparams['Vz']}
        self.sph_coord = dict()
        self.translate_to_sph()
        self.time_delta = targetparams['world_time_delta']
        self.time_delta_sq = pow(targetparams['world_time_delta'], 2)

        try:
            self.on_range = targetparams['gain']['range']
            self.on_range_gain = targetparams['gain']['value']
            self.get_gain = self._get_on_range_gain
        except KeyError:
            self.get_gain = self._get_static_gain

        try:
            self.rsc_sigma = targetparams['rsc_sigma']
        except KeyError:
            self.get_rsc = self._get_static_rsc

    def translate_to_sph(self):
        """
        Перевод Декартовых кооординат в сферические
        """
        self.sph_coord['r'] = math.sqrt(sum(map(lambda x: x ** 2, self.cart_coord.values())))
        self.sph_coord['pl'] = math.acos(self.cart_coord['z'] / self.sph_coord['r'])
        self.sph_coord['az'] = math.atan2(self.cart_coord['y'], self.cart_coord['x'])

    def radial_speed(self):
        """
        Метод возвращает радиальную скорость цели
        :return: радиальная скорость м/с
        """
        return ((self.cart_coord['x'] * self.speed_for_coord['Vx']) + (
            self.cart_coord['y'] * self.speed_for_coord['Vy']) + (self.cart_coord['z'] * self.speed_for_coord['Vz'])) / \
               self.sph_coord['r']

    def get_distance(self):
        """
        Интерфейм, возвращает дистанцию
        :return: дистанция, м
        """
        return self.sph_coord['r']

    def get_azimuth(self):
        """
        Возвращает азимут цели
        :return: азимут, радианы
        """
        return self.sph_coord['az']

    def get_place(self):
        """
        Возвращает угол места цели
        :return: угол места, радианы
        """
        return self.sph_coord['pl']

    def get_gain(self):
        """
        Интерфейс, возвращает усиление для цели
        """
        pass

    def _get_on_range_gain(self):
        """
        Рассчитываем усиление на основе заданного руками
        :return: усиление, безразмерное
        """
        # Амплитуда равна Аопрн * (Rопрн / R) ** 2
        return self.on_range_gain * ((self.on_range / self.get_distance()) ** 2)

    @staticmethod
    def _get_static_gain():
        return 100

    def get_rsc(self):
        """
        Возвращает случайное значение ЭПР распределенное по закону Рэлея(коррелированный шум)
        """
        return np.random.rayleigh(self.rsc_sigma)

    def _get_static_rsc(self):
        # Возвращает статическое значение ЭПР
        return self.rsc_sigma

