#!/usr/bin/pzthon
# -*- coding: utf-8 -*-
import itertools
import math
from helpers import prefixis

__author__ = 'trollknurr'

import numpy


class Locator(object):
    """
    Класс представляющий собой простой локатор с двумя возможными типами задания ДНА: простой sin(x)/x и табличный
     # !Ноль угла места в направлении оси z
    """
    def __init__(self, config):
        # Locator pos
         # !Ноль угла места в направлении оси z
        self.speed = numpy.radians(config["ROTATION_SPEED"])
        self.locator_angle = 0
        self.locator_place = numpy.radians(90)
        #Коэффициент усиления антенны = КПД * КИП * КНД * ДН
        self.locator_gain = config["DNA_KPD"] * config["DNA_KIP"] * 4 * math.pi * config["X_SIZE"] * config["Z_SIZE"]
        self.locator_gain /= pow(config["LAMBDA"], 2)
        # Время обного оборота
        self.full_rotate_time = 360. / config["ROTATION_SPEED"]
        self.receive_mode = config["RECEIVE_MODE"]

        self.x_angles = None
        self.z_angles = None

        self.x_values = None
        self.z_values = None

        dna_tzpe_prepare = {
            0: self._simple_dna,
            1: self._table_dna
        }

        prepare_func = dna_tzpe_prepare.get(config["DNA_TYPE"], 0)
        prepare_func(config)

    def _table_dna(self, config):
        """
        Считывание табличной диаграммы направленности
         # !Ноль угла места в направлении оси z
        """
        #Для подготовки массивов
        number_of_x_steps = len(config['DNA_TABLE_AZ'])
        number_of_z_steps = len(config['DNA_TABLE_PL'])

        self.x_values = numpy.zeros(number_of_x_steps)
        self.x_angles = numpy.zeros(number_of_x_steps)

        self.z_angles = numpy.zeros(number_of_z_steps)
        self.z_values = numpy.zeros(number_of_z_steps)

        for index, pair in enumerate(config['DNA_TABLE_AZ']):
            self.x_values[index] = pair['value']
            self.x_angles[index] = math.radians(pair['angle'])

        for index, pair in enumerate(config['DNA_TABLE_PL']):
            self.z_values[index] = pair['value']
            self.z_angles[index] = math.radians(pair['angle'])

        self.zero_x = abs(min(self.x_values))
        self.zero_z = abs(min(self.z_values))

    def _simple_dna(self, config):
        """
        Создание ДНА вида sin(x)/x
         # !Ноль угла места в направлении оси z
        """
        #Волновое число
        wave_number = numpy.pi / config["LAMBDA"]

        #Пределы диаграммы
        self.zero_x = math.radians(config['X_START'])
        self.zero_z = math.radians(config['Z_START'])

        #Множители из формулы sin(x)/x
        mul_x = wave_number * config["X_SIZE"]
        mul_z = wave_number * config["Z_SIZE"]

        #Углы
        self.x_angles = numpy.arange(-self.zero_x, self.zero_x, config['SIMPLE_STEP'])
        self.z_angles = numpy.arange(-self.zero_z, self.zero_z, config['SIMPLE_STEP'])

        #Значения
        self.x_values = numpy.zeros_like(self.x_angles)
        self.z_values = numpy.zeros_like(self.z_angles)

        #Забиваем массивы рассчитываемыми значениями
        for index, angle in enumerate(self.x_angles):
            self.x_values[index] = math.sin(mul_x * math.sin(angle)) / (mul_x * math.sin(angle))
        for index, angle in enumerate(self.z_angles):
            self.z_values[index] = math.sin(mul_z * math.sin(angle)) / (mul_z * math.sin(angle))



    def spin(self, world_time_delta):
        """
        Имитирует поворот ДНА
        :world_time_delta время, выделенное для поворота или период с которым моделируется мир
        """
        self.locator_angle = (self.locator_angle + self.speed * world_time_delta) % 6.28

    def gain_for_target(self, target):
        """
        Интерфейс для получения усиления ДНА для цели target
        :target объект Target или объект реализующий массив sph_coord с тремя сферическими координатами
                                                                                            дальность
                                                                                            угол места(0 сонаправлен с z)
                                                                                            азимут

        """
        if self.receive_mode:
            return self.locator_gain * self.gain_for_target_azimuth(target)
        return self.gain_for_target_azimuth(target)

    def gain_for_target_place(self, target):
        """
        Поиск усиления для цели по углу места. Интерполяция значения линейна
        """
        # !Ноль угла места в направлении оси z
        target_place = target.get_place() - self.locator_place
        if -self.zero_z < target_place < self.zero_z:
            for index, angel in enumerate(self.z_angles):
                if angel >= target_place:
                    # return self.z_values[index] + (self.z_values[index - 1] - self.z_values[index]) / (
                    #     self.z_angles[index] - self.z_angles[index - 1]) * (target_place - self.z_angles[index - 1])
                    return numpy.interp(target_place, self.z_angles[index - 1: index], self.z_values[index - 1: index])
        return 0

    def gain_for_target_azimuth(self, target):
        """
        Поиск усиления для цели по азимуту. Интерполяция значения линейна
        """
        target_azimuth = target.get_azimuth() - self.locator_angle
        if -self.zero_x < target_azimuth < self.zero_x:
            for index, angel in enumerate(self.x_angles):
                if angel >= target_azimuth:
                    # return self.x_values[index] + (self.x_values[index - 1] - self.x_values[index]) / (
                    #     self.x_angles[index] - self.x_angles[index - 1]) * (target_azimuth - self.x_angles[index - 1])
                    return numpy.interp(target_azimuth, self.x_angles[index - 1: index], self.x_values[index - 1: index])
        return False
