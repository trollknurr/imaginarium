#!/usr/bin/python
# -*- coding: utf-8 -*-

import struct
import socket
import time
import os
import datetime

__author__ = 'trollknurr'


class FileWriter(object):
    typeNames = {
        'int8': 'b',
        'uint8': 'B',
        'int16': 'h',
        'uint16': 'H',
        'int32': 'i',
        'uint32': 'I',
        'int64': 'q',
        'uint64': 'Q',
        'float': 'f',
        'double': 'd',
        'char': 's'}
    typeSizes = {
        'char': 1,
        'double': 8,
        'float': 4,
        'int16': 2,
        'int32': 4,
        'int64': 8,
        'int8': 1,
        'uint16': 2,
        'uint32': 4,
        'uint64': 8,
        'uint8': 1}

    def __init__(self, receive_array_len, steps_in_frame, file_name=u'data_drop.bin'):
        if os.path.exists(file_name):
            i = 0
            while True:
                i += 1
                dot_pos = file_name.find('.')
                name = file_name[:dot_pos] + u'_{}'.format(i) + file_name[dot_pos:]
                if not os.path.exists(name):
                    break
        else:
            name = file_name
        self.file = open(name, 'wb')
        try:
            self.frame_size = struct.pack('I', ((16 + 4 * receive_array_len) * steps_in_frame))
        except struct.error:
            raise IndexError

        self.header_string = '{flags}{start_type}{reserved0}{reserved1}{data_size}{angle}{sta_reserved}{time}'.format(
            flags=struct.pack('B', 36),
            start_type=struct.pack('B', 0),
            reserved0=struct.pack('B', 0),
            reserved1=struct.pack('B', 0),
            data_size=struct.pack('I', receive_array_len),
            angle='{angle}',
            sta_reserved=struct.pack('H', 0),
            time='{time}',
        )

    def new_frame(self):
        self.file.write(self.frame_size)
        self.file.flush()

    def write(self, angle, received_data):
        temp_string = self.header_string.format(
            angle=struct.pack('H', angle),
            time=struct.pack('I', int(time.time()))
        )
        self.file.write(temp_string)
        self.file.write(received_data)
        self.file.flush()

    def down(self):
        print "Closing file"
        self.file.close()


class ServerWriter(object):
    """
    Класс, отвечающий за передачу данных по сети. Является drop-in заменой FileWriter
    """
    log = '{size}{who}{zero}{when}{zero}{message}{zero}'
    zero = chr(0)
    buffer = ''
    frames_in_buffer = 0

    def __init__(self, receive_array_len, steps_in_frame):
        """
        Инициализация сервера
        :param receive_array_len:
        :param steps_in_frame:
        """
        self.manage_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.data_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        self.manage_sock.bind(('0.0.0.0', 2011))
        self.data_sock.bind(('0.0.0.0', 2012))

        self.manage_sock.listen(1)
        self.data_sock.listen(3)

        self.header_string = '{flags}{start_type}{reserved0}{reserved1}{data_size}{angle}{sta_reserved}{time}'.format(
            flags=struct.pack('B', 36),
            start_type=struct.pack('B', 0),
            reserved0=struct.pack('B', 0),
            reserved1=struct.pack('B', 0),
            data_size=struct.pack('I', receive_array_len),
            angle='{angle}',
            sta_reserved=struct.pack('H', 0),
            time='{time}',
        )

        self.buffer_string = '{header}{data}'
        self.server_data_string = '{size}{buffer_data}'
        self.listen_for_connetctions()


    def listen_for_connetctions(self):
        print "Listen for client on port 2011&2012"

        print "Listen for manage connection"
        self.manage_connection, client_address = self.manage_sock.accept()
        print "Manage socket opened: {}".format(self.manage_connection.recv(4))

        print "Listen for log connection"
        self.log_connection, client_address = self.data_sock.accept()
        print "Log socket opened: {}".format(self.log_connection.recv(4))

        print "Listen for tel connection"
        self.tel_connection, client_address = self.data_sock.accept()
        print "Telem socket opened: {}".format(self.tel_connection.recv(4))

        print "Listen for data connection"
        self.data_connection, client_address = self.data_sock.accept()
        print "Data socket opened: {}".format(self.data_connection.recv(4))

        message = 'Imaginarium server on air'
        who = 'Imaginarium server'
        when = str(datetime.datetime.now().time())

        hello_log = self.log.format(size=struct.pack('I', len(message) + len(who) + len(when) + len(self.zero) * 3),
                                    message=message,
                                    when=when,
                                    who=who,
                                    zero=self.zero)

        self.log_connection.sendall(hello_log)
        return True

    def new_frame(self):
        pass

    def write(self, angle, received_data):
        header = self.header_string.format(
            angle=struct.pack('H', angle),
            time=struct.pack('I', int(time.time()))
        )

        self.buffer += self.buffer_string.format(header=header,
                                                data=received_data)
        self.frames_in_buffer += 1
        if self.frames_in_buffer > 150:
            self.data_connection.sendall(self.server_data_string.format(size=struct.pack('I', len(self.buffer)),
                                                                        buffer_data=self.buffer))
            self.buffer = ''
            self.frames_in_buffer = 0

    def down(self):
        """
        Закрываем сетевые соединения
        """
        self.manage_sock.close()
        self.data_sock.close()