#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'trollknurr'

import numpy as np
import math
import cmath


class BaseReceiver(object):
    """
    Базовый класс для приемников всех типов.
    Реализует в себе однотипную конфигурацию, а так же общие для всех типов методы
    """

    def __init__(self, config):
        self.pulse_repeat_time = config["PULSE_REPEAT_TIME"]
        self.sampling_time_delta = 1. / config['FREQUENCY']
        self.mu = 0
        self.sigma = np.sqrt(1.38e-23 * np.pi * (
            300 * (1 + (config["RECEIVER_NOISE"] - 1) * config["RECEIVER_TRACT"] * config["ANTENNA_TRACT"])))
        self.receive_array_len = int(self.pulse_repeat_time / self.sampling_time_delta)

        self.received_data = np.zeros(self.receive_array_len, dtype=np.complex)

        self.pulse_duration = config["PULSE_DURATION"]
        self.wave_lambda = config["LAMBDA"]
        self.signal_freq = 0
        self.station_power = config["STATION_POWER"]
        self.attenuator = config["STATION_ATTENUATOR"]
        self.receiver_gain = config["RECEIVER_GAIN"]
        self.receive_mode = config["RECEIVE_MODE"]

        # Однозначная дальность обнаружения
        uniq_range = 1.5e+8 * config['PULSE_REPEAT_TIME']

        print("Uniq range = {}".format(uniq_range))

        if self.receive_mode == 0:
            self.target_amplitude = self._simple_target_amplitude
        else:
            self.target_amplitude = self._hard_target_amplitude


    def blank(self):
        """
        Инициализирует массив, представляющий собой реализии на выходе АЦП. Длиной соответствует периоду ожидания
        импульса, поделенного на период дескритизации.
        Заполняет массив отсчетами шума, представляющего собой весь шумы антенно-приемного тракта.
        Необходимо вызывать перед каждым началом приема
        """
        self.received_data[:] = np.random.normal(self.mu, self.sigma, self.receive_array_len) * 1j
        self.received_data[:] += np.random.normal(self.mu, self.sigma, self.receive_array_len)
        self.received_data *= self.receiver_gain

    def target_amplitude(self, loc_gain, target):
        pass

    def _simple_target_amplitude(self, loc_gain, target):
        # Используем опорные значения
        return loc_gain * target.get_gain() * self._reduce_signal(target.get_distance())

    def _hard_target_amplitude(self, loc_gain, target):
        # Основное уравнение радиолокации
        # Амплитуда на входе локатора: корень квадратный из _calculate_input_power и делим на 50 Ом
        # Домнажаем на коэффициент усиления УПЧ (хр-ка приемника) и на коэффициент ослабление аттенюатора
        return (math.sqrt(
            self._calculate_input_power(loc_gain, target)) / 2) * self.receiver_gain * self._reduce_signal(
            target.get_distance())

    def _calculate_input_power(self, loc_gain, target):
        """
        Решает основное уравнение радиолокации для получения уровня принятого от цели сигнала
        """
        #Основное уравнение радиолокации и работа аттенюатора
        return ((self.station_power * pow(loc_gain, 2) * pow(self.wave_lambda, 2) * target.get_rsc()) / (
            (4 * math.pi) ** 3 * target.get_distance() ** 4))

    def _reduce_signal(self, distance):
        """
        Имитирует работу аттенюатора, ослабляет сигнал в зависимости от времени прихода
        """
        prev_value = self.attenuator[0]['reduce']
        for el in self.attenuator:
            if el['range'] > distance:
                return 1. / prev_value
            else:
                prev_value = el['reduce']
        return 1. / prev_value

    def receive(self, direction_number, target_loc_gain, target, los_angle):
        """
        Интерфейс. Переопределить в наследнике.
        Предполагается когерентное накопление импульсов.
        :param direction_number:
        :param target_loc_gain:
        :param target:
        :param los_angle:
                :target_loc_gain усиление локатора для цели
        :target объект класса Target, или подобный, реализующий методы get_distance возвращающий дальность до цели
                                                                       get_rsc возвращающий ЭПР цели
        """
        pass

    def _doppler(self, t, target_radial_speed):
        """
        Рассчет допплеровского смещения фазы сигнала
        :param t: время
        :param target_radial_speed: радиальная скорость цели
        :return:
        """
        dopler_freq = 2 * target_radial_speed / self.wave_lambda
        return cmath.exp(- 1j * 6.28 * dopler_freq * t)

    @staticmethod
    def _heaviside(self, x):
        """
        Функция хевисайда
        :param x: координата х
        :return: возвращает единицу или ноль
        """
        if x < 0:
            return 0
        return 1

