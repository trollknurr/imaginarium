#!/usr/bin/python
# -*- coding: utf-8 -*-

import math
import cmath
import numpy
import struct

from .base import BaseReceiver

__author__ = 'trollknurr'


class RadioReceiver(BaseReceiver):
    """
    Класс, реализующий работу с обыкновенным радиоимпульсом
    """

    def receive(self, direction_number, target_loc_gain, target, los_angle):
        """
        Прием простого радиоимпульса
        :param los_angle: текущее угловое положение локатора
        :param direction_number: номер углового направления
        :param target_loc_gain: значение ДНА в направлении цели
        :param target: объект типа Target
        """
        #Задержка до прихода сигнала
        tau = 2. * target.get_distance() / 3e+8
        #Индекс приемного массива, с которого в него надо добавлять отсчеты принятого сигнала
        down = math.floor((tau + self.pulse_duration) / self.sampling_time_delta)
        #Солнышко скроется, приемник закроется
        if down < self.receive_array_len:
            #Не закрылся, ищем когда закончим добавлять отсчеты
            up = math.ceil((tau + 2 * self.pulse_duration) / self.sampling_time_delta)
            #Долго ли коротко ли, а не начнется ли новый импульс
            if up > self.receive_array_len:
                up = self.receive_array_len
            # Мы нашли значения в строке матрицы времени, когда у нас есть отсчеты сигнала
            # С помощью класса Time достаем эти моменты времени и считаем отсчеты
            current_time = self.time.get_time(direction_number, 0) - tau
            target_radial_speed = target.radial_speed() * math.cos(los_angle - target.get_azimuth())
            signal = self._harm_signal_function_with_dopler(current_time, target_radial_speed) * self.target_amplitude(
                target_loc_gain, target)
            for n in xrange(int(down), int(up), 1):
                self.received_data[n] += signal


    def _harm_signal_function(self, t):
        """
        Модель излученного сигнала
        :param t: время, с
        :return: отсчет сигнала в заданный момент времени
        """
        return cmath.exp(1j * 6.28 * self.signal_freq * t)

    def _harm_signal_function_with_dopler(self, t, target_radial_speed):
        """
        Модель принятого сигнала с допплеровским сдвигом частоты (время задержки вычитать при вызове)
        :param t: время, с
        :param target_radial_speed: радиальная скорость цели, м/с
        :return: отсчет сигнала в заданный момент времени
        """
        return self._harm_signal_function(t) * self._doppler(t, target_radial_speed)


