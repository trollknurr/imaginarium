#!/usr/bin/python
# -*- coding: utf-8 -*-
import math
import cmath

__author__ = 'trollknurr'

from .base import BaseReceiver


class LfmReceiver(BaseReceiver):
    """
    Класс, реализующий прием ЛЧМ радиоимпульса
    """

    def __init__(self, config):
        super(LfmReceiver, self).__init__(config)
        self.freq_dev = config['LFM_SIGNAL_SLOPE']
        self.lfm_signal_counts_len = int(self.pulse_duration / self.sampling_time_delta)

    def receive(self, direction_number, target_loc_gain, target, los_angle):
        """
        Принимаем ЛЧМ импульс. Тип определяется при инициализации
        :param los_angle: текущее угловое положение радиолокатора
        :param direction_number: номер углового направления
        :param target_loc_gain: значение ДНА в направлении цели
        :param target: объекти типа Target
        """
         #Задержка до прихода сигнала
        tau = 2. * target.get_distance() / 3e+8
        #Индекс приемного массива, с которого в него надо добавлять отсчеты принятого сигнала
        down = math.floor((tau + self.pulse_duration) / self.sampling_time_delta)
        lfm_signal_counts_len = self.lfm_signal_counts_len
        #Солнышко скроется, приемник закроется
        if down < self.receive_array_len:
            #Не закрылся, ищем когда закончим добавлять отсчеты
            up = down + lfm_signal_counts_len
            #Долго ли коротко ли, а не начнется ли новый импульс
            if up > self.receive_array_len:
                lfm_signal_counts_len = up - self.receive_array_len
            # Мы нашли значения в строке матрицы времени, когда у нас есть отсчеты сигнала
            # С помощью класса Time достаем эти моменты времени и считаем отсчеты
            current_time = self.time.get_time(direction_number, 0) - tau
            target_radial_speed = target.radial_speed() * math.cos(los_angle - target.get_azimuth())
            doppler = self._doppler(current_time, target_radial_speed)
            ampl = self.target_amplitude(target_loc_gain, target)
            for i in xrange(lfm_signal_counts_len):
                self.received_data[down + i] += self._direct_lfm_signal(self.time.get_time(0, i)) * doppler * ampl

    def _direct_lfm_signal(self, t):
        """
        Модель ЛЧМ импульса
        :param t: время
        :return: комплексный отсчет
        """
        return cmath.exp(-1j * (self.freq_dev / (2 * self.pulse_duration)) * pow(t, 2))
