#!/usr/bin/python
# -*- coding: utf-8 -*-

import numpy

__author__ = 'trollknurr'

def _receive_table(self, world_step, target_loc_gain, target):
    """
    Принимает сигнал, заданный таблично
    """
    #Задержка до прихода сигнала
    tau = 2. * target.get_distance() / 3e+8
    #Доплеровское смещение
    dopler = -1j * 2 * math.pi * (2. * target.radial_speed() / self.wave_lambda) * self.sampling_time_delta
    #Индекс приемного массива, с которого в него надо добавлять отсчеты принятого сигнала
    down = math.floor((tau + self.pulse_duration) / self.sampling_time_delta)
    #Солнышко скроется, приемник закроется
    if down < self.receive_array_len:
        #Не закрылся, ищем когда закончим добавлять отсчеты
        up = down + self.signal_array_len
        #Долго ли коротко ли, а не начнется ли новый импульс
        if up > self.receive_array_len:
            up = self.receive_array_len
            # Забиваем нужный нам интервал из массива отсчетов, который мы заготовили при инициализации
        numpy.add(  # Что складывать (весь массив с отсчетами сигнала или только его часть)
                    self.signal_array[:up] * self.target_amplitude(target_loc_gain, target),
                    self.received_data[down:up],  #С чем складывать
                    self.received_data[down:up])  #Куда сохранить результат

def _prepare_table(self, config):
    #Read raw data
    signal = config['TABLE_SIGNAL']
    signal_table_len = len(signal)
    self.signal_array_len = int(self.pulse_duration / self.sampling_time_delta)
    if signal_table_len == self.signal_array_len:
        self.signal_array = numpy.array(signal)
    elif self.signal_array_len > signal_table_len:
        zoom_rate = int(self.signal_array_len / signal_table_len)
        #indexes, treated as x-points.
        xp = list()
        #values, treated as y-points
        fp = list()
        # Zoom array
        for index, value in enumerate(signal):
            #Scale indexes
            xp.append(index * zoom_rate)
            #With same values
            fp.append(value)
        #Interpolate for receive need signal array len
        self.signal_array = numpy.interp(range(self.signal_array_len), xp, fp)
    else:
        #Zoom out amount as !int
        zoom_rate = int(signal_table_len / self.signal_array_len)
        #Just skip values, go through data with zoom out step
        self.signal_array = numpy.array([signal[i] for i in range(0, signal_table_len, zoom_rate)])