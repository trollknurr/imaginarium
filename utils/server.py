#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'trollknurr'

import struct
import socket

manage_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
data_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

manage_sock.bind(('localhost', 2011))
data_sock.bind(('localhost', 2012))

manage_sock.listen(1)
data_sock.listen(3)

manage_connection, client_address = manage_sock.accept()
data = manage_connection.recv(4)
print "Manage Data: {}".format(data)

log_connection, client_address = data_sock.accept()
print "Log Data: {}".format(log_connection.recv(4))
tel_connection, client_address = data_sock.accept()
print "Telem Data: {}".format(tel_connection.recv(4))
data_connection, client_address = data_sock.accept()
print "Data Connection: {}".format(data_connection.recv(4))

message = 'Hello Skirl!'
who = ' Custom server'
when = 'now'
zero = chr(0)
log = '{size}{who}{zero}{when}{zero}{message}{zero}'.format(
    size=struct.pack('I', len(message) + len(who) + len(when) + len(zero) * 3),
    message=message,
    when=when,
    who=who,
    zero=zero
)

log_connection.sendall(log)

while True:
    manage_connection.recv(4)