#!/usr/bin/python
# -*- coding: utf-8 -*-
import math
import cmath

__author__ = 'trollknurr'

import json
import matplotlib.pyplot as plt
import numpy as np

from helpers import Time

with open('../utils/default.json', 'r') as json_file:
    config = json.load(json_file)

pulse_desc_delta = 1. / config['FREQUENCY']

locator_angle_delta = 360. * config['THINNING'] / config['LOCATOR_ENCODER_RESOLUTION']

locator_time_step = locator_angle_delta / config['ROTATION_SPEED']

pulse_repeat_time = config['POROSITY'] * config['PULSE_DURATION']

n = pulse_repeat_time / pulse_desc_delta

k = config['LOCATOR_ENCODER_RESOLUTION'] / config['THINNING']

time = Time(pulse_repeat_time, pulse_desc_delta, k, locator_time_step)

# Параметры цели
target_range = 1000
radial_speed = -50
target_racus = np.pi

def get_target_range(time):
    return target_range - radial_speed * time

def get_locator_angle(time):
    return (config['ROTATION_SPEED'] / 180) * np.pi * time

angles = np.zeros(k)
for kk in range(k):
    angles[kk] = get_locator_angle(time.get_time(kk, 0))

# f, ax = plt.subplots()
# ax.plot(angles)
# ax.set_title(u"Locator's angle")

target_radial = np.zeros(k)
for kk in range(k):
    angle_delta = (angles[kk] - target_racus) % 6.28
    if 1.58 < angle_delta < 4.712:
        pass
    else:
        target_radial[kk] = radial_speed * math.cos(angle_delta)

doppler = np.array((2. * target_radial) / config['LAMBDA'])

# f, axarr = plt.subplots(2, sharex=True)
# axarr[0].plot(angles, target_radial)
# axarr[1].plot(angles, doppler)

receive_times = np.zeros(k)
target_ranges = np.zeros(k)

for kk in range(k):
    if target_radial[kk] > 0:
        current_time = time.get_time(kk, 0)
        target_range = get_target_range(current_time)
        pulse_delay = 2 * target_range / 3e+8
        receive_times[kk] = current_time + pulse_delay
        target_ranges[kk] = target_range

# f, axarr = plt.subplots(2, sharex=True)
# axarr[0].plot(receive_times)
# axarr[1].plot(target_ranges)

arg = np.zeros(k, dtype=np.complex)
signal = np.zeros(k, dtype=np.complex)

for kk in range(k):
    arg[kk] = 2 * math.pi * doppler[kk] * receive_times[kk]
    signal[kk] = cmath.exp(- 1j * arg[kk])

f, ax = plt.subplots()
ax.plot(arg)
ax.set_title('Exp argument')

f, axarr = plt.subplots(2, sharex=True)
axarr[0].plot(signal.real)
axarr[1].plot(signal.imag)



plt.show()